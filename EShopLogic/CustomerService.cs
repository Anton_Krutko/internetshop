﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using EShopData;
using EShopData.Entities;
using EShopLogic.Models;
using Microsoft.AspNetCore.Identity;

namespace EShopLogic
{
    public class CustomerService
    {
        ShopDbContext _db;
        public CustomerService(ShopDbContext shopDbContext)
        {
            _db = shopDbContext;
        }

        public async Task<IEnumerable<Customer>> GetAllCustomersAsync()
        {
            return await _db.Customers.ToListAsync();
        }
        private Customer GetCustomer(string id)
        {
            return _db.Customers.FirstOrDefault(x => x.Id == id);
        }
        public async Task<Customer> GetCustomerAsync(string id)
        {
            return await Task.Run(() => GetCustomer(id));
        }
        private Customer AddCustomer(RegisterModel customerModel)
        {

            Customer customer = new Customer{ Email = customerModel.Email, Password = customerModel.Password, Adress = customerModel.Adress };
            Cart cart = new Cart()
            {
                Customer = customer
            };
            _db.Customers.Add(customer);
            _db.Carts.Add(cart);
            _db.SaveChanges();
            return customer;
        }
        public async Task<Customer> AddCustomerAsync(RegisterModel registerModel)
        {
            return await Task.Run(() => AddCustomer(registerModel));
        }
        private Customer UpdateCustomer(Customer customer)
        {
            _db.Update(customer);
            _db.SaveChanges();
            return customer;
        }
        public async Task<Customer> UpdateCustomerAsync(Customer customer)
        {
            return await Task.Run(() => UpdateCustomer(customer));
        }
        /*private bool IsUnique(Customer customer)
        {
            if (_db.Customers.Select(c => c.Email).Any(c => c == customer.Email))
            {
                return false;
            }
            return true;
        }
        public async Task<bool> IsUniqueAsync(Customer customer)
        {
            return await Task.Run(() => IsUnique(customer));
        }*/
        public async Task<bool> IsIncluded(string Id)
        {
            if ( await _db.Customers.AnyAsync(x => x.Id == Id))
            {
                return true;
            }
            return false;
        }
    }
}
