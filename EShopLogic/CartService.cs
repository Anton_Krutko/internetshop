﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EShopData;
using EShopData.Entities;
using Microsoft.EntityFrameworkCore;

namespace EShopLogic
{
    public class CartService
    {
        public readonly ShopDbContext _db;

        public CartService(ShopDbContext dBContent)
        {
            _db = dBContent;
        }
        private Cart AddProduct(int productid, int cartid)
        {
            var product = _db.Products.FirstOrDefault(x => x.Id == productid);
            if (product.Quantity == 0 || product == null || _db.Carts.FirstOrDefault(c => c.Id == cartid) == null)
            {
                return null;
            }
            if (_db.CartItems.Contains(_db.CartItems.FirstOrDefault(c => c.ProductId == productid && c.CartId == cartid)))
            {
                _db.CartItems.FirstOrDefault(c => c.ProductId == productid && c.CartId == cartid).Quantity += 1;
                _db.Products.FirstOrDefault(x => x.Id == productid).Quantity -= 1;
            }
            else
            {
                _db.Carts.FirstOrDefault(x => x.Id == cartid).CartItems.Add(new CartItem { CartId = cartid, ProductId = productid, Quantity = 1 });
                _db.Products.FirstOrDefault(x => x.Id == productid).Quantity -= 1;
            }
            
            _db.SaveChanges();
            return _db.Carts.FirstOrDefault(x => x.Id == cartid);
        }
        public async Task<Cart> AddProductAsync(int productId, int cartId)
        {
            return await Task.Run(() => AddProduct(productId, cartId));
        }
        public async Task<Cart> GetCartAsync(int id)
        {
            return await _db.Carts.FirstOrDefaultAsync(c => c.Id == id);
        }
        private List<Product> GetProducts(int cartId)
        {
            if (_db.Carts.FirstOrDefault(c => c.Id == cartId) == null)
            {
                return null;
            }
            List<CartItem> cartItems = _db.CartItems.Where(c => c.CartId == cartId).ToList();
            List<Product> products = new List<Product>();
            foreach (var item in cartItems)
            {
                products.Add(_db.Products.FirstOrDefault(p => p.Id == item.ProductId));
            }
            return products;
        }
        public async Task<List<Product>> GetProductsAsync(int cartId)
        {
            return await Task.Run(() => GetProducts(cartId));
        }
        private Cart DeleteProduct(int cartId, int productId)
        {
            if (_db.Carts.FirstOrDefault(c => c.Id == cartId) == null || _db.Products.FirstOrDefault(p => p.Id == productId) == null || _db.CartItems.FirstOrDefault(c => c.CartId == cartId && c.ProductId == productId) == null)
            {
                return null;
            }

            int quantity = _db.CartItems.FirstOrDefault(c => c.ProductId == productId).Quantity;

            if (quantity <= 1)
            {
                _db.CartItems.Remove(_db.CartItems.FirstOrDefault(c => c.ProductId == productId && c.CartId == cartId));
                _db.Products.FirstOrDefault(x => x.Id == productId).Quantity += 1;
            }
            else
            {
                _db.CartItems.FirstOrDefault(c => c.CartId == cartId && c.ProductId == productId).Quantity -= 1;
                _db.Products.FirstOrDefault(x => x.Id == productId).Quantity += 1;
            }

            _db.SaveChanges();
            return _db.Carts.FirstOrDefault(c => c.Id == cartId);
        }
        public async Task<Cart> DeleteProductAsync(int cartId, int productId)
        {
            return await Task.Run(() => DeleteProduct(cartId, productId));
        }
        private Cart ClearCart(int cartId)
        {
            foreach (var item in _db.Carts.Include(c => c.CartItems).FirstOrDefault(x => x.Id == cartId).CartItems)
            {
                _db.Products.FirstOrDefault(x => x.Id == item.ProductId).Quantity += item.Quantity;
            }
            _db.Carts.Include(c => c.CartItems).FirstOrDefault(x => x.Id == cartId).CartItems.Clear();
            _db.SaveChanges();
            return _db.Carts.FirstOrDefault(x => x.Id == cartId);
        }
        public async Task<Cart> ClearCartAsync(int cartId)
        {
            return await Task.Run(() => ClearCart(cartId));
        }
    }
}
