﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EShopLogic.Models
{
    public class CartIdProductId
    {

        public int cartId { get; set; }
        public int productId { get; set; }
    }
}
