﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using EShopData;
using EShopData.Entities;

namespace EShopLogic
{
    public class ProductService
    {
        ShopDbContext _db;
        public ProductService(ShopDbContext shopDbContext)
        {
            _db = shopDbContext;
        }
        public async Task<IEnumerable<Product>> GetAllProductsAsync()
        {
            return await _db.Products.ToListAsync();
        }
        private Product GetProduct(int id)
        {
            return _db.Products.FirstOrDefault(x => x.Id == id);
        }
        public async Task<Product> GetProductAsync(int id)
        {
            return await Task.Run(() => GetProduct(id));
        }
        private Product AddProduct(Product product)
        {
            _db.Products.Add(product);
            _db.SaveChanges();
            return product;
        }
        public async Task<Product> AddProductAsync(Product product)
        {
            return await Task.Run(() => AddProduct(product));
        }
        private Product UpdateProduct(Product product)
        {
            _db.Update(product);
            _db.SaveChanges();
            return product;
        }
        public async Task<Product> UpdateProductAsync(Product product)
        {
            return await Task.Run(() => UpdateProduct(product));
        }
        public async Task<bool> IsIncluded(int productId)
        {
            if (await _db.Products.AnyAsync(x => x.Id == productId))
            {
                return true;
            }
            return false;
        }
    }
}
