﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EShopData.Entities
{
    public class Order
    {
        public int Id { get; set; }
        public int CartId { get; set; }
        public Cart Cart { get; set; }
        public int TotalCost { get; set; }
        public string Adress { get; set; }
        public DateTime Date { get; set; }
    }
}
