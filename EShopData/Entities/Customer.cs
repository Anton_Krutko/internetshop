﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace EShopData.Entities
{
    public class Customer : IdentityUser
    {
        public string Password { get; set; }
        public string Adress { get; set; }
        public Cart Cart { get; set; }
    }
}
