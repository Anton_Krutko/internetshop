﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using EShopData.Entities;

using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
namespace EShopData
{
        public class ShopDbContext : IdentityDbContext<Customer>
        {
            public ShopDbContext(DbContextOptions<ShopDbContext> options) : base (options)
            {
                //Database.EnsureDeleted();
                Database.EnsureCreated();
            }
            protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            {

            }
            protected override void OnModelCreating(ModelBuilder modelBuilder)
            {
                base.OnModelCreating(modelBuilder);
            //modelBuilder.Entity<Cart>().HasMany(c => c.Products).WithMany(c => c.Carts);
            // modelBuilder.Entity<Customer>().HasOne(c => c.Cart).WithOne(c => c.Customer).HasForeignKey<Cart>(c => c.CustomerId);
        }

            public DbSet<Cart> Carts { get; set; }
            public DbSet<Customer> Customers { get; set; }
            public DbSet<Order> Orders { get; set; }
            public DbSet<Product> Products { get; set; }
            public DbSet<CartItem> CartItems { get; set; }
        }
}
