﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using EShopData.Entities;
using EShopData;
using EShopLogic;
using System.Linq;
using EShopLogic.Models;
using System.Collections.Generic;

namespace EShopAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class CartController : ControllerBase
    {
        CartService cartService;
        ProductService productService;
        public CartController(ShopDbContext context, CartService cartService, ProductService productService)
        {
            this.cartService = cartService;
            this.productService = productService;
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<Cart>> Get(int id)
        {
            Cart cart = await cartService.GetCartAsync(id);
            if (cart == null)
                return NotFound();
            return Ok(cart);
        }
        [HttpPost("/AddProduct")]
        public async Task<ActionResult<Cart>> Add([FromBody] CartIdProductId cartIdProductId)
        {
            var product = await productService.GetProductAsync(cartIdProductId.productId);
            var cart = await cartService.GetCartAsync(cartIdProductId.cartId);
            if (cart == null || product == null)
            {
                return NotFound();
            }
            await cartService.AddProductAsync(cartIdProductId.productId, cartIdProductId.cartId);
            return Ok(cart);
        }
        [HttpPut("/DeleteProduct")]
        public async Task<ActionResult<Cart>> Delete([FromBody] CartIdProductId cartIdProductId)
        {
            var product = await productService.GetProductAsync(cartIdProductId.productId);
            var cart = await cartService.GetCartAsync(cartIdProductId.cartId);
            if (cart == null || product == null)
            {
                return NotFound();
            }
            cart = await cartService.DeleteProductAsync(cartIdProductId.cartId, cartIdProductId.productId);
            return Ok(cart);
        }
        [HttpPut("/ClearCart")]
        public async Task<ActionResult<Cart>> Clear(int id)
        {
            Cart cart = await cartService.ClearCartAsync(id);
            if (cart == null)
            {
                return NotFound();
            }
            return Ok(cart);
        }
        [HttpGet("/ShowProducts")]
        public async Task<ActionResult<IEnumerable<Product>>> ShowProducts(int cartId)
        {
            return await cartService.GetProductsAsync(cartId);
        }
    }
}
