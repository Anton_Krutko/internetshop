﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using static EShopData.ShopDbContext;
using EShopData.Entities;
using EShopData;
using EShopLogic;
using EShopLogic.Models;

namespace EShopAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        CustomerService CustomerService;
        public CustomerController(ShopDbContext context, CustomerService customerService)
        {
            CustomerService = customerService;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Customer>>> Get()
        {
            return Ok(await CustomerService.GetAllCustomersAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Customer>> Get(string id)
        {
            Customer customer = await CustomerService.GetCustomerAsync(id); ;
            if (customer == null)
                return NotFound();
            return Ok(customer);
        }

        [HttpPost("/Register")]
        public async Task<ActionResult<Customer>> Register(RegisterModel customer)
        {
            /*if (customer == null || await CustomerService.IsUniqueAsync(customer) == false)
            {
                return BadRequest();
            }*/
            await CustomerService.AddCustomerAsync(customer);
            return Ok(customer);
        }

        [HttpPut]
        public async Task<ActionResult<Customer>> Put(Customer customer)
        {
            if (customer == null)
            {
                return BadRequest();
            }
            if (await CustomerService.IsIncluded(customer.Id) == false)
            {
                return NotFound();
            }
            await CustomerService.UpdateCustomerAsync(customer);
            return Ok(customer);
        }
    }
}
